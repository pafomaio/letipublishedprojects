//
// File Person
// Created by Paulo Maio on 2021.
//

#ifndef TASKS_PERSON_H
#define TASKS_PERSON_H
#include <string>
#include <memory>
#include "CategoryContainer.h"

using namespace std;

class Person {
private:
    wstring name;
    shared_ptr<CategoryContainer> categories = make_shared<CategoryContainer>();
    bool isNameValid(const wstring &name);
public:
    Person(const wstring &name);
    const wstring& getName() const;
    shared_ptr<CategoryContainer> getCategoriesContainer();
};


#endif //TASKS_PERSON_H
