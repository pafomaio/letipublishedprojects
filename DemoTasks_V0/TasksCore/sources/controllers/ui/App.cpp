//
// File App
// Created by Paulo Maio on 2021.
//

#include "headers/controllers/ui/App.h"

using namespace std;

/**
 * Static methods should be defined outside the class.
 */

App* App::pinstance_{nullptr};
std::mutex App::mutex_;

/**
 * Instance Methods
 */
App::App() {
    this->person = make_shared<Person>(L"Paulo");
}

App::~App() {

}
shared_ptr<Person> App::getPerson() {
    return this->person;
}

/**
 * The first time we call GetInstance we will lock the storage location
 *      and then we make sure again that the variable is null and then we
 *      set the value. RU:
 */
App *App::GetInstance() {
    std::lock_guard<std::mutex> lock(mutex_);
    if (pinstance_ == nullptr)
    {
        pinstance_ = new App();
    }
    return pinstance_;
}



