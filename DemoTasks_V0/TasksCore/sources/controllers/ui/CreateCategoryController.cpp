//
// File CreateCategoryController
// Created by Paulo Maio on 2021.
//

#include "headers/controllers/ui/App.h"
#include "headers/controllers/ui/CreateCategoryController.h"


void CreateCategoryController::createCategory(const wstring &code, const wstring &description) {
    shared_ptr<CategoryContainer> container =  this->person->getCategoriesContainer();
    this->category = container->create(code,description);
}

Result CreateCategoryController::saveCreatedCategory() {
    if (this->category){
        shared_ptr<CategoryContainer> container =  this->person->getCategoriesContainer();
        return container->save(this->category);
    }
    return Result::NOK(L"A category should be created first.");
}

Result CreateCategoryController::createAndSaveCategory(const wstring &code, const wstring &description) {
    return Result::NOK(L"not implemented yet");
}




