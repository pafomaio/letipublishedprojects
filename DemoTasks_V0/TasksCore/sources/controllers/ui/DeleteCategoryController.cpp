//
// File DeleteCategoryController
// Created by Paulo Maio on 2021.
//

#include "headers/controllers/ui/DeleteCategoryController.h"
#include <vector>

const vector<shared_ptr<Category>> DeleteCategoryController::getAll() const {
    shared_ptr<CategoryContainer> container =  this->person->getCategoriesContainer();
    list<shared_ptr<Category>> list = container->getAll();
    vector<shared_ptr<Category>> categories;
    /** Transforming a list into a vector --> adapting to the UI needs
     *  This differs from what was done on the UpdateCategoryController to show some diversity
     *  and explores the responsibilities of Controllers (i.e. serving as adapters)
    */
    for(shared_ptr<Category> cat:list)
        categories.push_back(cat);
    //
    return categories;
}

Result DeleteCategoryController::deleteCategory(const wstring &code) {
    shared_ptr<CategoryContainer> container =  this->person->getCategoriesContainer();
    optional<shared_ptr<Category>> obj = container->findById(code);
    if (obj.has_value())
    {
        shared_ptr<Category> cat = obj.value();
        return container->remove(cat);
    }
    return Result::NOK(L"Category not found.");
}
