//
// File ListCategoriesController
// Created by Paulo Maio on 2021.
//

#include "headers/controllers/ui/ListCategoriesController.h"

const list<shared_ptr<Category>> ListCategoriesController::getAll() const {
    shared_ptr<CategoryContainer> container =  this->person->getCategoriesContainer();
    return container->getAll();
}
