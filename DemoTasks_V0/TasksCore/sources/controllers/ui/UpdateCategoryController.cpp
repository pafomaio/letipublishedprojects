//
// File UpdateCategoryController
// Created by Paulo Maio on 2021.
//

#include "headers/controllers/ui/UpdateCategoryController.h"

const list<shared_ptr<Category>> UpdateCategoryController::getAll() const {
    shared_ptr<CategoryContainer> container =  this->person->getCategoriesContainer();
    return container->getAll();
}

Result UpdateCategoryController::updateCategory(const wstring &code, const wstring &description) {
    shared_ptr<CategoryContainer> container =  this->person->getCategoriesContainer();
    optional<shared_ptr<Category>> obj = container->findById(code);
    if (obj.has_value())
    {
        shared_ptr<Category> cat = obj.value();
        Result result = cat->changeDescription(description);
        if (result.isOK())
            return container->save(cat);
        else
            return result;
    }
    return Result::NOK(L"Category not found.");
}
