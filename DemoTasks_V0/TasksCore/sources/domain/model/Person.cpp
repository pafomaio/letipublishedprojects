//
// File Person
// Created by Paulo Maio on 2021.
//

#include "headers/domain/model/Person.h"
#include "headers/domain/shared/StringUtils.h"
#include "headers/domain/exceptions/TaskDomainError.h"
using namespace std;

Person::Person(const wstring &name) {
    if (!this->isNameValid(name))
        throw TaskDomainError("Invalid value for a person name.");

    this->name = StringUtils::trim(name);
}

bool Person::isNameValid(const wstring &name) {
    return StringUtils::ensureNotNullOrEmpty(name);
}


shared_ptr<CategoryContainer> Person::getCategoriesContainer()  {
    return this->categories;
}

const wstring &Person::getName() const {
    return this->name;
}


