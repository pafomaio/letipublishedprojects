//
// File PersonTests
// Created by Paulo Maio on 2021.
//

#include "gtest/gtest.h"
#include <headers/domain/model/Person.h>
#include <headers/domain/exceptions/TaskDomainError.h>


class PersonFixture : public ::testing::Test {

protected:
    virtual void SetUp(){
        // Add here some testing set up code
    }

    virtual void TearDown() {
        // Add here some testing tear down code
    }

};

TEST_F(PersonFixture, CreateWithNonValidName){
    EXPECT_THROW(new Person(L"  "),TaskDomainError);
    EXPECT_THROW(new Person(L""),TaskDomainError);
}

TEST_F(PersonFixture, CreateWithValidName){
    EXPECT_NO_THROW(new Person(L"John"));
}

TEST_F(PersonFixture, CheckingNameHasNoLeftAndRightSpaces){
    Person p(L" Jo hn ");

    EXPECT_EQ(p.getName(),L"Jo hn");
}

TEST_F(PersonFixture, CheckingManipulatingCategoriesContainer){
    Person p(L" John ");

    shared_ptr<CategoryContainer>  container = p.getCategoriesContainer();
    EXPECT_EQ(container->count(),0);

    container->save(container->create(L"C9090",L"Category 9090"));
    EXPECT_EQ(container->count(),1);

    container = p.getCategoriesContainer();
    EXPECT_EQ(container->count(),1);
}
