# README #

This repository contains didactic artifacts to support and demonstrate some principles, concepts, approaches, methods and practices lectured in the [Software Engineering (ESOFT)](https://portal.isep.ipp.pt/intranet/education/visualiza_ficha_uc_v7.aspx?cde=43326) course unit of the [Bachelor in Telecommunications and Informatics Engineering (LETI)](https://www.isep.ipp.pt/Course/Course/473) from [Instituto Superior de Engenharia do Porto (ISEP)](http://www.isep.ipp.pt).

### What is this repository for? ###

This repository aims to be used by students as an **entry point** to their _initial studies_ regarding ESOFT.

For that purpose, it comprehends a (very) small demo project regarding the development of an information system. In particular, it has:

* The initial Problem Statement
* Requirements Engineering Artifacts
    * Glossary
    * Use Case Diagram
    * Supplementary Specification (using FURPS+)
    * User Stories (US)
* OO Analysis
    * Domain Model
* Design Artifacts (per US)
    * Sequence Diagram (SD)
    * Class Diagram (CD)
* Automatic Regression Tests
    * Unit Tests
    * Integration Tests
* Code/Implementation (using C++) of some functionalities (US)


**The organization present on this repository/project should also be adopted by students while developing the project by which they will be assessed/evaluated to ESOFT.**

At last, it should be noticed that, throughout the semester, these artifacts and code will naturally evolve to capture/reflect the software engineering competencies acquired in the meantime.

### How do I get set up? ###

* Download/Clone the repository
* Open it using [CLion](https://www.jetbrains.com/clion/)
* Project (DemoTasks) and sub-projects (TasksCore, TasksCoreTests and TasksConsoleApp) are loaded and ready to be compiled and executed
    * DemoTasks is just an umbrella to the other projects. Running it, forces the other projects to be compiled.
    * TasksCore is a **library** where the domain logic and entities reside.
    * TasksCoreTests has set of regression tests over the TasksCore classes.  
    * TasksConsoleApp is an executable console application providing a basic User Interface (UI) allowing users to perform some application domain functionalities.
* All projects are configured/compiled using [CMake](https://cmake.org) files.
    * A quick CMake tutorial can be found [here](https://www.jetbrains.com/help/clion/quick-cmake-tutorial.html).
* For convenience in reading/editing UML artifacts, it is required to install the [PlantUML plugin](https://plugins.jetbrains.com/plugin/7017-plantuml-integration).

### Where do I start? ###

* Read the initial [Problem Statement](docs/ProblemStatement.md)
* Read/update the [team members information and carried out tasks](docs/TeamMembersAndTasks.md).
* From the tasks' distribution table follow the link to the provided artifacts.
* It is recommended starting by analysing the Requirements Engineering Artifacts.
* Further, verify how users' functionalities are addressed/fulfilled in Design artifacts.
* Finally,  examine the code to testify how well the proposed design is being satisfied.

### Who do I talk to? ###

* Paulo Maio (pam@isep.ipp.pt)
