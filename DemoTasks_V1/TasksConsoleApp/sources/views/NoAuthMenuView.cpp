//
// File NoAuthMenuView
// Created by Paulo Maio on 2021.
//

#include "../../headers/views/NoAuthMenuView.h"
#include <vector>

using namespace std;

NoAuthMenuView::NoAuthMenuView() {
    this->headers = {L"Anonymous User Options!"};

    this->menuOptions = {
            L"Application Goal",
            L"Credits"
    };

    this->cancelMenuMsg = L"Return";
}

int NoAuthMenuView::processMenuOption(int option) {
    int result = 0;
    vector<wstring> lines;
    switch (option) {
        case 1:
            lines = {
                    L"This application was developed in the scope of LETI-ESOFT lectured at ISEP.",
                    L"It aims to be used by students:",
                    L"\t-to recap their competencies presumably acquired on preceding course units (e.g. APROG, FSOFT)",
                    L"\t-as a starting point to acquire new competencies."
            };
            utils.printLines(lines);
            utils.readEnterToContinue();
            break;
        case 2:
            lines = {
                L"LETI-ESOFT Faculty Members:",
                L"\tPaulo Maio (pam@isep.ipp.pt)"
            };
            utils.printLines(lines);
            utils.readEnterToContinue();
            break;
        default:
            result = -1;
            break;
    }

    return result;
}
