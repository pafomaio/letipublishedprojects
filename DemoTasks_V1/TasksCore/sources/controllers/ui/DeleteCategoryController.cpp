//
// File DeleteCategoryController
// Created by Paulo Maio on 2021.
//

#include "headers/controllers/ui/DeleteCategoryController.h"
#include <vector>

const vector<shared_ptr<Category>> DeleteCategoryController::getAll() const {
    shared_ptr<CategoryService> service = this->person->getCategoriesService();
    list<shared_ptr<Category>> list = service->getAll();
    vector<shared_ptr<Category>> categories;
    /** Transforming a list into a vector --> adapting to the UI needs
     *  This differs from what was done on the UpdateCategoryController to show some diversity
     *  and explores the responsibilities of Controllers (i.e. serving as adapters)
    */
    for(shared_ptr<Category> cat:list)
        categories.push_back(cat);
    //
    return categories;
}

Result DeleteCategoryController::deleteCategory(const wstring &code) {
    shared_ptr<CategoryService> service = this->person->getCategoriesService();
    return service->remove(code);
}
