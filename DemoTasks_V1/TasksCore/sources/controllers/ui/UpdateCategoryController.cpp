//
// File UpdateCategoryController
// Created by Paulo Maio on 2021.
//

#include "headers/controllers/ui/UpdateCategoryController.h"

const list<shared_ptr<Category>> UpdateCategoryController::getAll() const {
    shared_ptr<CategoryService> service = this->person->getCategoriesService();
    return service->getAll();
}

Result UpdateCategoryController::updateCategory(const wstring &code, const wstring &description) {
    shared_ptr<CategoryService> service = this->person->getCategoriesService();
    return service->changeDescription(code, description);
}
