project(TasksCoreTests)
add_subdirectory(lib)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR} ${gmock_SOURCE_DIR}/include ${gmock_SOURCE_DIR})

add_executable(TasksCoreTests domain/model/CategoryTests.cpp infrastructure/memory/list/CategoryListRepositoryTests.cpp domain/shared/StringUtilsTests.cpp domain/model/PersonTests.cpp controllers/ui/CreateCategoryControllerTests.cpp domain/repositories/RepositoryFactoryMock.h domain/repositories/CategoryRepositoryMock.h domain/services/CategoryServiceMock.h domain/services/CategoryServiceTests.cpp)

target_link_libraries(TasksCoreTests TasksCore)

target_link_libraries(TasksCoreTests gmock gtest gtest_main)