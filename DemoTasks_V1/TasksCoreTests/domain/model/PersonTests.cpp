//
// File PersonTests
// Created by Paulo Maio on 2021.
//
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <headers/domain/model/Person.h>
#include <headers/domain/exceptions/TaskDomainError.h>
#include "../repositories/RepositoryFactoryMock.h"


class PersonFixture : public ::testing::Test {

protected:
    virtual void SetUp(){
        // Add here some testing set up code
    }

    virtual void TearDown() {
        // Add here some testing tear down code
    }

};

TEST_F(PersonFixture, CreateWithNonValidName){
    EXPECT_THROW(new Person(L"  ", make_shared<RepositoryFactoryMock>()),TaskDomainError);
    EXPECT_THROW(new Person(L"", make_shared<RepositoryFactoryMock>()),TaskDomainError);
}

TEST_F(PersonFixture, CreateWithValidName){
    EXPECT_NO_THROW(new Person(L"John",make_shared<RepositoryFactoryMock>()));
}

TEST_F(PersonFixture, CheckingNameHasNoLeftAndRightSpaces){
    Person p(L" Jo hn ",make_shared<RepositoryFactoryMock>());

    EXPECT_EQ(p.getName(),L"Jo hn");
}


