//
// File CategoryServiceTests
// Created by Paulo Maio on 2021.
//
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <headers/domain/exceptions/TaskDomainError.h>
#include <headers/domain/services/CategoryService.h>
#include "../repositories/CategoryRepositoryMock.h"
#include <optional>

using ::testing::Return;
using ::testing::_;

class CategoryServiceFixture : public ::testing::Test {

protected:
    virtual void SetUp(){
        // Add here some testing set up code
        // Add here some testing set up code
    }

    virtual void TearDown() {
        // Add here some testing tear down code
    }

};

TEST_F(CategoryServiceFixture, CreateWithInvalidData){

    shared_ptr<CategoryRepositoryMock> repo = make_shared<CategoryRepositoryMock>();
    shared_ptr<CategoryService> service = make_shared<CategoryService>(repo);

    EXPECT_THROW(service->create(L"",L"Category One"),std::invalid_argument);
}

TEST_F(CategoryServiceFixture, CreateWithValidData){

    shared_ptr<CategoryRepositoryMock> repo = make_shared<CategoryRepositoryMock>();
    shared_ptr<CategoryService> service = make_shared<CategoryService>(repo);

    EXPECT_NO_THROW(service->create(L"C0001",L"Category One"));
}

TEST_F(CategoryServiceFixture, AddOrUpdateWithError){

    shared_ptr<CategoryRepositoryMock> repo = make_shared<CategoryRepositoryMock>();
    shared_ptr<CategoryService> service = make_shared<CategoryService>(repo);

    EXPECT_CALL(*repo, save(_))
            .Times(1)
            .WillOnce(Return(Result::NOK(L"")));
    shared_ptr<Category> cat = service->create(L"C0001",L"Category One");
    EXPECT_TRUE(service->addOrUpdate(cat).isNOK());
}

TEST_F(CategoryServiceFixture, AddOrUpdateWithSuccess){

    shared_ptr<CategoryRepositoryMock> repo = make_shared<CategoryRepositoryMock>();
    shared_ptr<CategoryService> service = make_shared<CategoryService>(repo);

    EXPECT_CALL(*repo, save(_))
            .Times(1)
            .WillOnce(Return(Result::OK()));
    shared_ptr<Category> cat = service->create(L"C0001",L"Category One");
    EXPECT_TRUE(service->addOrUpdate(cat).isOK());
}

TEST_F(CategoryServiceFixture, RemoveNonExisting){

    shared_ptr<CategoryRepositoryMock> repo = make_shared<CategoryRepositoryMock>();
    shared_ptr<CategoryService> service = make_shared<CategoryService>(repo);

    wstring code = L"C0001";
    EXPECT_CALL(*repo, getById(code))
            .Times(1)
            .WillOnce(Return(nullopt));
    EXPECT_CALL(*repo, remove(_))
            .Times(0);
    EXPECT_TRUE(service->remove(code).isNOK());
}

TEST_F(CategoryServiceFixture, RemoveExisting){

    shared_ptr<CategoryRepositoryMock> repo = make_shared<CategoryRepositoryMock>();
    shared_ptr<CategoryService> service = make_shared<CategoryService>(repo);

    wstring code = L"C0001";
    shared_ptr<Category> cat = make_shared<Category>(code,code);
    EXPECT_CALL(*repo, getById(code))
            .Times(1)
            .WillOnce(Return(make_optional<shared_ptr<Category>>(cat)));
    EXPECT_CALL(*repo, remove(cat))
            .Times(1)
            .WillOnce(Return(Result::OK()));
    EXPECT_TRUE(service->remove(code).isOK());
}

TEST_F(CategoryServiceFixture, ChangingDescriptionToNonExistingCategory){

    shared_ptr<CategoryRepositoryMock> repo = make_shared<CategoryRepositoryMock>();
    shared_ptr<CategoryService> service = make_shared<CategoryService>(repo);

    wstring code = L"C0001";
    EXPECT_CALL(*repo, getById(code))
            .Times(1)
            .WillOnce(Return(nullopt));
    EXPECT_CALL(*repo, save(_))
            .Times(0);
    EXPECT_TRUE(service->changeDescription(code,L"").isNOK());
}

TEST_F(CategoryServiceFixture, ChangingDescriptionToInvalid){

    shared_ptr<CategoryRepositoryMock> repo = make_shared<CategoryRepositoryMock>();
    shared_ptr<CategoryService> service = make_shared<CategoryService>(repo);

    wstring code = L"C0001";
    shared_ptr<Category> cat = make_shared<Category>(code,code);

    EXPECT_CALL(*repo, getById(code))
            .Times(1)
            .WillOnce(Return(make_optional<shared_ptr<Category>>(cat)));
    EXPECT_CALL(*repo, save(_))
            .Times(0);
    EXPECT_TRUE(service->changeDescription(code,L"").isNOK());
}

TEST_F(CategoryServiceFixture, ChangingDescriptionToValid){

    shared_ptr<CategoryRepositoryMock> repo = make_shared<CategoryRepositoryMock>();
    shared_ptr<CategoryService> service = make_shared<CategoryService>(repo);

    wstring code = L"C0001";
    shared_ptr<Category> cat = make_shared<Category>(code,code);
    EXPECT_CALL(*repo, getById(code))
            .Times(1)
            .WillOnce(Return(make_optional<shared_ptr<Category>>(cat)));
    EXPECT_CALL(*repo, save(cat))
            .Times(1)
            .WillOnce(Return(Result::OK()));
    EXPECT_TRUE(service->changeDescription(code,L"new code").isOK());
}