# Sprint A Entry Point

1. [Glossary](Glossary.md)
2. [Use Case Diagram](UCD.md)
3. [Supplementary Specification](FURPS.md)
4. [Domain Model](DM.md)