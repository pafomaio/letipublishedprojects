# Describing App Initialization

## 1. Overview of the Conceived Approach

* The _App_ is a singleton
* The method _"GetInstance"_ is **static**. Notice it is being called on the class and not to an App instance (the **":"** denotes an instance)
* The _App instance_ is being created in a lazy load fashion, i.e., it is created just when needed by the first time.
* The concrete _FactoryRepository_ to be used is determined according to the configured information existing on the "config.ini" file which can be changed during the system implantation/deployment.
* Further, the created _FactoryRepository_ is used to provide the object person a way to get its own repositories.

![AppInit](AppInit.svg)


**It is a best practice passing dependencies of a given object (e.g.: Person) through its constructor.**

This best practice fosters maintainability and testability. 