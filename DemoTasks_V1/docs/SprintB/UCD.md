# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and course units might also exist _1 to N **and/or** N to 1 relationships between UC and US.

**Insert below the Use Case Diagram in a SVG format**

By analyzing the problem statement, it is possible **to infer** the use cases depicted in the following diagram.

![Use Case Diagram](UCD.svg)

**It is worth noting that (some of) these UC were inferred. Therefore, it is important to validate its existence with the SW Client as well as in what each UC actually consists of.**


After receiving the User Stories of Sprint 1, the previous Use Case Diagram was updated in conformity as follows.

![Use Case Diagram](UCD_S1.svg)

**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

(Tip: organize the content of each US in a dedicated folder) 

# Use Cases / User Stories
| UC/US  | Description                                                               |                   
|:----|:------------------------------------------------------------------------|
| US01 | [To Create Category](US01/US01.md)   |
| US02 | [To List Categories](US02/US02.md)   |
| US03 | [To Update Category Description](US03/US03.md)  |
| US04 | [To Delete Category](US04/US04.md)   |
| US05 | [To Create Task](US05/US05.md)   |

