# US 01 - To create a task

## 1. Requirements Engineering

### 1.1. User Story Description

As a Person, I want to define a new task that I have to complete.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> A task is characterized by having a unique reference, a title, an informal description, and another of a more technical nature, and effort estimation as well as the deadline to be accomplished and the category in which it fits in.

**From the client clarifications:**

> **Question:** ?
>
> **Answer:** *

### 1.3. Acceptance Criteria

* AC05-1. Task reference, title, and category are mandatory. The remaining data is optional.

### 1.4. Found out Dependencies

* There is a dependency with [US01](../US01/US01.md) (and [US02](../US02/US02.md)) since categories must be previously created in order to categorize the task being created.


### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * an unique reference
    * a title
    * an informal description
    * a technical description
    * an effort estimation
    * a deadline

* Selected data:
    * category (used to classify the task)


**Output Data:**

* The list of existing categories
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

![US05-SSD](US05_SSD.svg)


### 1.7 Other Relevant Remarks

* The created task is defined and classified.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US05-MD](US05-MD.svg)

### 2.2. Other Remarks

n/a

## 3. Design - User Story Realization 

### 3.1. Rationale

n/a

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Person
 * Task
 * Category

Other software classes (i.e. Pure Fabrication) identified: 
 * CreateTaskView  
 * CreateTaskController
 * CategoryService
 * TaskService
 * CategoryRepository
 * TaskRepository
 * RepositoryFactory

## 3.2. Sequence Diagram (SD)

**Details of Getting/Creating Services from/on the class Person**

This diagram depicts and clarifies how _"Service Instances"_ are obtained by any _Controller_.
As so, such details can be omitted of further sequence diagrams.

![SD-CreatingServices](SD_CreatingServices.svg)

**Alternative 1**

![US05-SD](US05-SD.svg)

**This alternative is the one that best fits what was captured in the provided SSD (cf. above).**

However, other alternatives are plausible to be adopted. Some of those alternatives are depicted below.

**Alternative 2**

![US05-SD_v2](US05_SD_v2.svg)

**Notice the usage of a DTO to send the task data from the UI layer to the Domain layer.**

**Alternative 3**

![US05-SD_v3](US05_SD_v3.svg)

**Notice that user confirmation is requested before creating the Task object. Hence, the Service is able to create and immediately save the task.**

**Alternative 4**

![US05-SD_v4](US05_SD_v4.svg)


## 3.3. Class Diagram (CD)

**to be completed by students**


# 4. Tests

**to be completed by students**

# 5. Integration and Demo 

**to be completed by students**

# 6. Observations

n/a





