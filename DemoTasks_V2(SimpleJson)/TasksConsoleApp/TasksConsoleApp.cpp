//
// File TasksConsoleApp
// Created by Paulo Maio on 2021.
//
#include <iostream>
#include "headers/views/WelcomeView.h"

using namespace std;

int main() {

    WelcomeView view;
    view.show();
    return 0;
}