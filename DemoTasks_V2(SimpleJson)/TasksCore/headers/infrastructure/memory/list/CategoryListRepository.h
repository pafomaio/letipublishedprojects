//
// File CategoryListRepository
// Created by Paulo Maio on 2021.
//

#ifndef TASKS_CATEGORYLISTREPOSITORY_H
#define TASKS_CATEGORYLISTREPOSITORY_H

#include <list>
#include <optional>
#include <memory>
#include "headers/domain/model/Category.h"

#include "headers/domain/repositories/CategoryRepository.h"
#include "headers/infrastructure/memory/list/ListRepository.h"

using namespace std;

class CategoryListRepository : public CategoryRepository, public ListRepository<Category,CategoryId>  {
private:
public:
    CategoryListRepository();
    virtual optional<shared_ptr<Category>> getById(const wstring& code);
};


#endif //TASKS_CATEGORYLISTREPOSITORY_H
