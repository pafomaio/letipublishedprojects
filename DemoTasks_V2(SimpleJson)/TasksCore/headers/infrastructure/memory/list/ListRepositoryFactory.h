//
// File ListRepositoryFactory
// Created by Paulo Maio on 2021.
//

#ifndef TASKS_LISTREPOSITORYFACTORY_H
#define TASKS_LISTREPOSITORYFACTORY_H

#include "headers/domain/repositories/RepositoryFactory.h"
#include "CategoryListRepository.h"

class ListRepositoryFactory : public RepositoryFactory {
private:
    shared_ptr<CategoryRepository> categories = make_shared<CategoryListRepository>();
public:
    ListRepositoryFactory();
    virtual shared_ptr<CategoryRepository> getCategoriesRepo() override;
};


#endif //TASKS_LISTREPOSITORYFACTORY_H
