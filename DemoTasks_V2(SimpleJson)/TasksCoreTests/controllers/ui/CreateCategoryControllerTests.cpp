//
// File CreateCategoryControllerTests
// Created by Paulo Maio on 2021.
//
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <headers/controllers/ui/CreateCategoryController.h>
#include <headers/domain/model/Person.h>
#include <memory>
#include "../../domain/services/CategoryServiceMock.h"
#include "../../domain/repositories/RepositoryFactoryMock.h"
#include "../../domain/repositories/CategoryRepositoryMock.h"

using namespace std;
using ::testing::Return;
using ::testing::_;


/** \brief Mock for the person class.
 */
class PersonMock : public Person
{
public:
    PersonMock(): Person(L"Joe", make_shared<RepositoryFactoryMock>()){
    };

    MOCK_METHOD(shared_ptr<CategoryService>, getCategoriesService, (), (override));
private:

};


class CreateCategoryControllerFixture : public ::testing::Test {

protected:
    virtual void SetUp(){
        // Add here some testing set up code
    }

    virtual void TearDown() {
        // Add here some testing tear down code
    }

};

TEST_F(CreateCategoryControllerFixture, SavingWithoutCreatingCategory){

    shared_ptr<PersonMock> person = make_shared<PersonMock>();

    CreateCategoryController controller(person,L"BlaBla");

    Result result = controller.saveCreatedCategory();

    EXPECT_TRUE(result.isNOK());
}

TEST_F(CreateCategoryControllerFixture, SavingAfterCreatingCategory){

    shared_ptr<CategoryServiceMock> service = make_shared<CategoryServiceMock>();
    EXPECT_CALL(*service, create(_,_))
            .Times(1)
            .WillOnce(Return(make_shared<Category>(L"C0001", L"Category1")));
    EXPECT_CALL(*service, addOrUpdate(_))
        .Times(1)
        .WillOnce(Return(Result::OK()));

    shared_ptr<PersonMock> person = make_shared<PersonMock>();
    EXPECT_CALL(*person, getCategoriesService())
            .Times(2)
            .WillRepeatedly(Return(service));

    CreateCategoryController controller(person,L"BlaBla");
    controller.createCategory(L"C0001", L"Category 1");
    Result result = controller.saveCreatedCategory();

    EXPECT_TRUE(result.isOK());
}

