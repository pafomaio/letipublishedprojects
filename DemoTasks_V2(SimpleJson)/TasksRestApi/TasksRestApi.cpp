//
// File TasksRestApi
// Created by Paulo Maio on 2021.
//


#include "headers/TasksServer.h"

int main() {

    TasksServer server;
    server.run();

    return 0;
}

