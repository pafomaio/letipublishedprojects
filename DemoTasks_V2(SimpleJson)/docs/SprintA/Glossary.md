# Glossary

**Terms, Expressions and Acronyms (TEA) must be organized alphabetically.**


| **_TEA_** (EN)  | **_Description_** (EN)                                           |                                       
|:------------------------|:--------------------------------------------|
| **Category** | Corresponds to a concept/description used by the user to categorize/classify its tasks. |
| **GRASP** | Acronym for _General Responsibility Assignment Software Principles (or Patterns)_.|
| **I&I** | Acronym for _Iterative and Incremental_. |
| **JoeProfiles** | It is the name of an existing software system in use at the S4J whose main features are related with users registration processes and data.|
| **OO** |  Acronym for _Object-Oriented_.|
| **Person** | Corresponds to an Individual (_Registered User_) using the system being developed. It is also used to capture his/her personal data .|
| **Registered User** | A term used to refer to someone (usually a _Person_) with the ability to use the system being developed after the system knowing her/his identity by means of an authentication process. It is a _Person_ responsible for carrying out various business supporting activities on the system.|
| **S4J** |  Acronym for _Software for Joe_. It is the name of company requesting the development of the intended software product.|
| **SDP** |  Acronym for _Software Development Process_.|
| **SOLID** | An acronym used for referring to 5 software design principles: (i) Single Responsibility Principle; (ii) Open-Closed Principle; (iii) Liskov Substitution Principle; (iv) Interface Segregation Principle; and (v) Dependency Inversion Principle.|
| **Task** | Corresponds to a concept/description of something that a user has to do until a given deadline.|
| **TDD** | Acronym for _Test-Driven Development_.|
| **UI** |  Acronym for _User Interface_.|
| **Unregistered User** | A term used to refer to someone (usually a _Person_) with the ability to use the system being developed without the system knowing her/his identity since (s)he has not yet performed an authentication process. A short and basic set of feature are available.|
| **User** |  A generic term used to refer to someone (usually a _Person_) with the ability to use the system being developed. By omission, it refers to a _Registered User_. |












