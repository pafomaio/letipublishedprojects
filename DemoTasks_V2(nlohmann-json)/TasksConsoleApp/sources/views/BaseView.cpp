//
// File BaseView
// Created by Paulo Maio on 2021.
//

#include "../../headers/views/BaseView.h"
#include <iostream>

using namespace  std;

BaseView::BaseView() {
    locale::global(locale(""));
    wcout.imbue(locale());
}

void BaseView::renderHeader() {
    this->utils.printEmptyLines(5);
    this->utils.printLines(headers);
    this->utils.printEmptyLines(2);
}

int BaseView::show() {
    this->renderHeader();
    this->renderBody();
    return 0;
}


