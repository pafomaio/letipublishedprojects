//
// File HttpResult
// Created by Paulo Maio on 2021.
//

#ifndef TASKS_HTTPRESULT_H
#define TASKS_HTTPRESULT_H

#include "HttpStatus.h"
#include "thirdparty/json.hpp"
#include <string>

using namespace std;
using json = nlohmann::json;

class HttpResult {
private:
    HttpStatus status;
    json result;
public:
    HttpResult();
    HttpResult(HttpStatus status);
    HttpResult(HttpStatus status, const wstring &message);
    HttpResult(HttpStatus status, const string &message);
    HttpResult(HttpStatus status, const char *message);
    HttpResult(HttpStatus status, const json &result);
    HttpStatus getHttpStatus();
    json getResult();
    void setHttpStatus(HttpStatus status);
    void setResult(const wstring &message);
    void setResult(const string &message);
    void setResult(const char *message);
    void setResult(const json &result);

};


#endif //TASKS_HTTPRESULT_H
