//
// File CategoryMapper
// Created by Paulo Maio on 2021.
//

#ifndef TASKS_CATEGORYMAPPER_H
#define TASKS_CATEGORYMAPPER_H

#include "thirdparty/json.hpp"
#include "../../../domain/model/Category.h"
#include <string>
#include <list>

using namespace std;
using json = nlohmann::json;

class CategoryMapper {
public:
    static json toJson(shared_ptr<Category> obj);
    static json toJson(list<shared_ptr<Category>> objs);
};


#endif //TASKS_CATEGORYMAPPER_H
