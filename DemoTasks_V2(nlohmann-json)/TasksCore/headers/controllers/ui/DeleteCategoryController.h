//
// File DeleteCategoryController
// Created by Paulo Maio on 2021.
//

#ifndef TASKS_DELETECATEGORYCONTROLLER_H
#define TASKS_DELETECATEGORYCONTROLLER_H

#include <vector>
#include "AuthController.h"
#include "../../domain/model/Person.h"
#include "../../domain/model/Category.h"

using namespace std;

class DeleteCategoryController : public AuthController {
private:
    shared_ptr<Category> category;
public:
    DeleteCategoryController(const wstring &userToken): AuthController(userToken){};
    DeleteCategoryController(shared_ptr<Person> person, const wstring &userToken): AuthController(person,userToken){};
    const vector<shared_ptr<Category>> getAll() const;
    Result deleteCategory(const wstring &code);
};




#endif //TASKS_DELETECATEGORYCONTROLLER_H
