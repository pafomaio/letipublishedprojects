//
// File CategoryMapper
// Created by Paulo Maio on 2021.
//

#include "headers/controllers/restapi/mappers/CategoryMapper.h"

#include "thirdparty/json.hpp"
#include <list>
#include "headers/domain/shared/StringUtils.h"

using namespace std;
using json = nlohmann::json;

json CategoryMapper::toJson(shared_ptr<Category> obj) {
    json result = json();
    result["code"] = StringUtils::toString(obj->getCode());
    result["description"] = StringUtils::toString(obj->getDescription());
    return result;
}

json CategoryMapper::toJson(list<shared_ptr<Category>> objs) {
    json result = json::array();
    for(shared_ptr<Category> cat: objs){
       result.push_back(CategoryMapper::toJson(cat));
    }
    return result;
}
