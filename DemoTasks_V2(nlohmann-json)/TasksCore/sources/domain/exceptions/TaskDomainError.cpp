//
// File TaskDomainError
// Created by Paulo Maio on 2021.
//

#include "headers/domain/exceptions/TaskDomainError.h"

TaskDomainError::TaskDomainError(const string &what_arg) : domain_error(what_arg) {

}
