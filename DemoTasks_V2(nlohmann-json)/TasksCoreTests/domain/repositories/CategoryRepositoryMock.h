//
// File CategoryRepositoryMock
// Created by Paulo Maio on 2021.
//

#ifndef TASKS_CATEGORYREPOSITORYMOCK_H
#define TASKS_CATEGORYREPOSITORYMOCK_H

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <headers/domain/model/Category.h>
#include <headers/domain/repositories/CategoryRepository.h>

/** \brief Mock for the CategoryListRepository class.
 */
class CategoryRepositoryMock : public CategoryRepository
{
public:

    MOCK_METHOD(optional<shared_ptr<Category>>, getById, (const wstring&),(override));

    MOCK_METHOD(optional<shared_ptr<Category>>, getById, (shared_ptr<CategoryId>),(override));

    MOCK_METHOD(Result, save, (shared_ptr<Category>),(override));

    MOCK_METHOD(Result, remove, (shared_ptr<Category>),(override));

    MOCK_METHOD(list<shared_ptr<Category>>, getAll, (),(override));

    MOCK_METHOD(bool, isEmpty,(),(override));

    MOCK_METHOD(long, count,(),(override));
};




#endif //TASKS_CATEGORYREPOSITORYMOCK_H
