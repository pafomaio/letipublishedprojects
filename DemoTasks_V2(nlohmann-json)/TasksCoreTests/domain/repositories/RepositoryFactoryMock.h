//
// File RepositoryFactoryMock
// Created by Paulo Maio on 2021.
//

#ifndef TASKS_REPOSITORYFACTORYMOCK_H
#define TASKS_REPOSITORYFACTORYMOCK_H

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <headers/domain/repositories/RepositoryFactory.h>
#include <headers/domain/repositories/CategoryRepository.h>

/** \brief Mock for the RepositoryFactory class.
 */
class RepositoryFactoryMock : public RepositoryFactory
{
public:
    MOCK_METHOD(shared_ptr<CategoryRepository>, getCategoriesRepo, (),(override));
};


#endif //TASKS_REPOSITORYFACTORYMOCK_H
