//
// File CategoryServiceMock
// Created by Paulo Maio on 2021.
//

#ifndef TASKS_CATEGORYSERVICEMOCK_H
#define TASKS_CATEGORYSERVICEMOCK_H

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <headers/domain/model/Category.h>
#include <headers/domain/services/CategoryService.h>

/** \brief Mock for the CategoryService class.
 */
class CategoryServiceMock : public CategoryService
{
public:
    CategoryServiceMock(): CategoryService(nullptr){};

    MOCK_METHOD(shared_ptr<Category>, create, (const wstring&, const wstring&),(override));

    MOCK_METHOD(Result, addOrUpdate, (shared_ptr<Category>),(override));

    MOCK_METHOD(Result, remove, (const wstring &),(override));

    MOCK_METHOD(Result, changeDescription, (const wstring&, const wstring&),(override));

    MOCK_METHOD(list<shared_ptr<Category>>, getAll, (),(override));

};

#endif //TASKS_CATEGORYSERVICEMOCK_H
