//
// File CategoryContainerTests
// Created by Paulo Maio on 2021.
//

#include "gtest/gtest.h"
#include <headers/domain/model/Category.h>
#include <headers/domain/repositories/CategoryRepository.h>
#include <headers/infrastructure/memory/list/CategoryListRepository.h>



class CategoryListRepositoryFixture : public ::testing::Test {

protected:
    virtual void SetUp(){
        // Add here some testing set up code
        repo = new CategoryListRepository();
    }

    virtual void TearDown() {
        // Add here some testing tear down code
    }

    void populateWithFourCategories(){
        EXPECT_TRUE(this->repo->isEmpty());
        cat1 = make_shared<Category>(L"C0019", L"Category 19");
        cat2 = make_shared<Category>(L"C0020", L"Category 20");
        cat3 = make_shared<Category>(L"C0021", L"Category 21");
        cat4 = make_shared<Category>(L"C0022", L"Category 22");
        this->repo->save(cat1);
        this->repo->save(cat2);
        this->repo->save(cat3);
        this->repo->save(cat4);
        EXPECT_EQ(this->repo->count(),4);
    }

    CategoryListRepository * repo;
    shared_ptr<Category> cat1, cat2, cat3, cat4;
};

TEST_F(CategoryListRepositoryFixture, CreateWithValidArguments){

    shared_ptr<Category> cat = make_shared<Category>(L"C0019", L"Category 19");
    EXPECT_EQ(cat->getCode(),L"C0019");
}

TEST_F(CategoryListRepositoryFixture, CreateWithInvalidArguments){
    EXPECT_THROW(make_shared<Category>(L"C19", L"Category 19"),invalid_argument);
}

TEST_F(CategoryListRepositoryFixture, SavingNulls){

    EXPECT_FALSE(this->repo->save(nullptr).isOK());
    EXPECT_TRUE(this->repo->isEmpty());

    EXPECT_FALSE(this->repo->save(NULL).isOK());
    EXPECT_TRUE(this->repo->isEmpty());

    shared_ptr<Category> ptr;
    EXPECT_FALSE(this->repo->save(ptr).isOK());
    EXPECT_TRUE(this->repo->isEmpty());
}

TEST_F(CategoryListRepositoryFixture, RemovingNulls){

    this->populateWithFourCategories();

    EXPECT_FALSE(this->repo->remove(nullptr).isOK());
    EXPECT_EQ(this->repo->count(),4);

    EXPECT_FALSE(this->repo->remove(NULL).isOK());
    EXPECT_EQ(this->repo->count(),4);

    shared_ptr<Category> ptr;
    EXPECT_FALSE(this->repo->remove(ptr).isOK());
    EXPECT_EQ(this->repo->count(),4);
}

TEST_F(CategoryListRepositoryFixture, GetAllWhenEmpty){

    EXPECT_TRUE(this->repo->isEmpty());

    list<shared_ptr<Category>> list = this->repo->getAll();

    EXPECT_TRUE(list.empty());
}

TEST_F(CategoryListRepositoryFixture, GetAllWhenPopulated){

    this->populateWithFourCategories();

    list<shared_ptr<Category>> list = this->repo->getAll();

    EXPECT_EQ(list.size(), 4);
}

TEST_F(CategoryListRepositoryFixture, AddingOneCategory){
    EXPECT_TRUE(this->repo->isEmpty());
    shared_ptr<Category> cat = make_shared<Category>(L"C0019", L"Category 19");
    this->repo->save(cat);
    EXPECT_FALSE(this->repo->isEmpty());
}

TEST_F(CategoryListRepositoryFixture, AddingDuplicatedCategory){

    EXPECT_TRUE(this->repo->isEmpty());
    shared_ptr<Category> cat1 = make_shared<Category>(L"C0019", L"Category 19");
    shared_ptr<Category> cat2 = make_shared<Category>(L"C0019", L"Category C0019");
    this->repo->save(cat1);
    EXPECT_EQ(this->repo->count(),1);
    EXPECT_TRUE(this->repo->save(cat1).isOK());
    EXPECT_EQ(this->repo->count(),1);
    EXPECT_FALSE(this->repo->save(cat2).isOK());
    EXPECT_EQ(this->repo->count(),1);
}

TEST_F(CategoryListRepositoryFixture, AddingMultipleCategories){
    this->populateWithFourCategories();
}

TEST_F(CategoryListRepositoryFixture, RemovingCategory){

    this->populateWithFourCategories();

    EXPECT_TRUE(this->repo->remove(cat2).isOK());
    EXPECT_EQ(this->repo->count(),3);
    optional<shared_ptr<Category>> obj = this->repo->getById(L"C020");
    EXPECT_FALSE(obj.has_value());
}

TEST_F(CategoryListRepositoryFixture, RemovingNonExistingCategory){

    this->populateWithFourCategories();

    shared_ptr<Category> obj = make_shared<Category>(L"C0099", L"Category 99");
    EXPECT_FALSE(this->repo->remove(obj).isOK());
    EXPECT_EQ(this->repo->count(),4);
}

TEST_F(CategoryListRepositoryFixture, RemovingTwiceSameCategory){

    this->populateWithFourCategories();

    EXPECT_TRUE(this->repo->remove(cat3).isOK());
    EXPECT_EQ(this->repo->count(),3);
    EXPECT_FALSE(this->repo->remove(cat3).isOK());
}

TEST_F(CategoryListRepositoryFixture, FindingCategoriesById){

    this->populateWithFourCategories();

    optional<shared_ptr<Category>> obj = this->repo->getById(L"C031");
    EXPECT_FALSE(obj.has_value());

    obj = this->repo->getById(L"C0019");
    EXPECT_TRUE(obj.has_value());
    EXPECT_EQ(obj.value(),cat1);

    obj = this->repo->getById(L"C0020");
    EXPECT_TRUE(obj.has_value());
    EXPECT_EQ(obj.value(),cat2);

    obj = this->repo->getById(L"C0021");
    EXPECT_TRUE(obj.has_value());
    EXPECT_EQ(obj.value(),cat3);

    obj = this->repo->getById(L"C0022");
    EXPECT_TRUE(obj.has_value());
    EXPECT_EQ(obj.value(),cat4);
}
