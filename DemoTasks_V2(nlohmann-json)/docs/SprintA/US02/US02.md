# US 02 - To list categories

## 1. Requirements Engineering

### 1.1. User Story Description

As a Person, I want to see a list of all existing categories.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> a user might categorize tasks using a set of categories maintained by him/her.
> By simplicity, a category just comprehends a unique alphanumeric code and a brief description.

**From the client clarifications:**

> **Question:** Is it necessary to provide sorting and filtering mechanisms?
>
> **Answer:** "Such mechanisms are desired but not during sprint 1." (cf.[here](.))

### 1.3. Acceptance Criteria

* Not-Provided
* No implicit AC were found

### 1.4. Found out Dependencies

* No dependencies were found. 
* However, categories must be previously created ([US01](../US01/US01.md))

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * n/a

* Selected data:
    * n/a


**Output Data:**

* List of Categories

### 1.6. System Sequence Diagram (SSD)


![US02-SSD](US02-SSD.svg)


### 1.7 Other Relevant Remarks

* n/a


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US02-MD](US02-MD.svg)

### 2.2. Other Remarks

n/a

## 3. Design - User Story Realization 

### 3.1. Rationale

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |	... interacting with the actor? | ListCategoryView   |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.           |
| 			  		 |	... coordinating the US? | ListCategoryController | Controller                             |
| 			  		 |	... knows all existing categories? | Person   | IE: Person maintains Category (cf. DM)  |
| 			  		 |	                                  | CategoryContainer   | By applying High Cohesion (HC) + Low Coupling (LC) on class Person, it delegates the responsibility on CategoryContainer.   |
| 			  		 | ... knows the CategoryContainer?   | Person  | IE: Person knows the CategoryContainer to which it is delegating some responsibilities.  |
| 			  		 | ... knows each category data?   | Category  | IE: Each Category knows its own data.  |
| Step 2  		 |	... listing all categories? | ListCategoryView            |  IE: is responsible for user interactions.    |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Person
 * Category

Other software classes (i.e. Pure Fabrication) identified: 
 * ListCategoryView  
 * ListCategoryController
 * CategoryContainer

## 3.2. Sequence Diagram (SD)

![US02-SD](US02-SD.svg)

**Doubts:**

* Does it make any sense the UI (View) accessing directly our domain objects (i.e. Category)?
* What are the risks of doing that?
* Is there a way to prevent such thing of happening?

## 3.3. Class Diagram (CD)

![US02-CD](US02-CD.svg)

**Note: private attributes and/or methods were omitted.**

# 4. Tests

Two relevant test scenarios are highlighted next.
Other test were also specified.

**Test 1:** Getting all existing categories when no one exists. 

     TEST_F(CategoryContainerFixture, GetAllWhenEmpty){
        EXPECT_TRUE(this->container->isEmpty());
        list<shared_ptr<Category>> list = this->container->getAll();
        EXPECT_TRUE(list.empty());
    }
    

**Test 2:** Getting all existing categories when several exists.

    TEST_F(CategoryContainerFixture, GetAllWhenPopulated){
        this->populateWithFourCategories();
        list<shared_ptr<Category>> list = this->container->getAll();
        EXPECT_EQ(list.size(), 4);
    }


# 5. Integration and Demo 

A menu option on the console application was added. Such option invokes the ListCategoryView.


    int CategoriesMenuView::processMenuOption(int option) {
        int result = 0;
        BaseView * view;
        switch (option) {
          ...
          case 2:
            view = new ListCategoryView(this->userToken);
            view->show();
            break;
          ...
        }
        return result;
    }

# 6. Observations

n/a





