# US 02 - To list categories

## 1. Requirements Engineering

### 1.1. User Story Description

As a Person, I want to see a list of all existing categories.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

> a user might categorize tasks using a set of categories maintained by him/her.
> By simplicity, a category just comprehends a unique alphanumeric code and a brief description.

**From the client clarifications:**

> **Question:** Is it necessary to provide sorting and filtering mechanisms?
>
> **Answer:** "Such mechanisms are desired but not during sprint 1." (cf.[here](.))

### 1.3. Acceptance Criteria

* Not-Provided
* No implicit AC were found

### 1.4. Found out Dependencies

* No dependencies were found. 
* However, categories must be previously created ([US01](../US01/US01.md))

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * n/a

* Selected data:
    * n/a


**Output Data:**

* List of Categories

### 1.6. System Sequence Diagram (SSD)


![US02-SSD](US02-SSD.svg)


### 1.7 Other Relevant Remarks

* n/a


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US02-MD](US02-MD.svg)

### 2.2. Other Remarks

n/a

## 3. Design - User Story Realization 

### 3.1. Rationale

n/a

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Person
 * Category

Other software classes (i.e. Pure Fabrication) identified: 
 * ListCategoryView  
 * ListCategoryController
 * CategoryService
 * CategoryRepository
 * RepositoryFactory


**Notice that several recently lectured principles and patterns are being applied now.**

## 3.2. Sequence Diagram (SD)

### 3.2.1. Previous Perspective (using Controllers for a Console UI)

**On this matter, Sprint C add no changes here.**

![US02-SD](US02-SD.svg)

**Doubts:**

* Does it make any sense the UI (View) accessing directly our domain objects (i.e. Category)?
* What are the risks of doing that?
* Is there a way to prevent such thing of happening?

  
  **To prevent this, DTOs should be adopted.**

### 3.2.2. REST API Perspective (reusing the domain logic)

**To accomodate the Sprint C requirements.**

![US02-SD-RestAPI](US02-SD-RestAPI.svg)

## 3.3. Class Diagram (CD)

### 3.3.1. Previous Perspective (using Controllers for a Console UI)

![US02-CD](US02-CD.svg)

**Note: private attributes and/or methods were omitted.**

### 3.3.2. REST API Perspective (reusing the domain logic)

**To accomodate the Sprint C requirements.**

Just the new classes are shown.

![US02-CD-RestAPI](US02-CD-RestAPI.svg)

# 4. Tests

Two relevant test scenarios to a concrete CategoryRepository implementation are highlighted next.
Other tests were also specified.

**Test 1:** Getting all existing categories when no one exists. 

     TEST_F(CategoryListRepositoryFixture, GetAllWhenEmpty){
          EXPECT_TRUE(this->repo->isEmpty());
          list<shared_ptr<Category>> list = this->repo->getAll();
          EXPECT_TRUE(list.empty());
      }
    

**Test 2:** Getting all existing categories when several exists.

    TEST_F(CategoryListRepositoryFixture, GetAllWhenPopulated){
        this->populateWithFourCategories();
        list<shared_ptr<Category>> list = this->repo->getAll();
        EXPECT_EQ(list.size(), 4);
    }


# 5. Integration and Demo 

A menu option on the console application was added. Such option invokes the ListCategoryView.


    int CategoriesMenuView::processMenuOption(int option) {
        int result = 0;
        BaseView * view;
        switch (option) {
          ...
          case 2:
            view = new ListCategoryView(this->userToken);
            view->show();
            break;
          ...
        }
        return result;
    }

# 6. Observations

n/a





