//
// File BaseAuthView
// Created by Paulo Maio on 2021.
//

#include "../../headers/views/BaseAuthView.h"

using namespace std;

BaseAuthView::BaseAuthView(const wstring &userToken): BaseView() {
    this->userToken = userToken;
}
