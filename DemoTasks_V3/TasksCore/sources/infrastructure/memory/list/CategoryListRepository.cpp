//
// File CategoryListRepository
// Created by Paulo Maio on 2021.
//

#include "headers/infrastructure/memory/list/CategoryListRepository.h"

using namespace std;


CategoryListRepository::CategoryListRepository(): ListRepository<Category, CategoryId>(){
}

optional<shared_ptr<Category>> CategoryListRepository::getById(const wstring& code) {
    for (list<shared_ptr<Category>>::iterator it = container.begin(); it != container.end(); ++it){
        shared_ptr<Category> obj = *it;
        if (obj->hasCode(code))
            return make_optional<shared_ptr<Category>>(obj);
    }
    return nullopt;
}












