# Describing Generic Flow an Any HTTP Request

## 1. Sequence Diagram 

**This diagram depicts the flow (i.e. sequence of actions/steps) that any HTTP Request will follow.**

![GenericSD](GenericSD.svg)

#### Some highlights/assumptions:

1. The Controller receives data/information extracted by the RouteHandler from the HTTPRequest.
2. Any Controller method invoked return an HTTP Result object.
3. An HTTPResult object states the proper HTTP Status to return and, optionally, a JSON object to include in the HTTPResponse.
4. The RouteHandler updates the HTTPResponse in accordance with the HTTPResult return by the Controller.

Accordingly, the Controllers are not dependent of the HTTP library (_httplib.h_) adopted to handle the communications.
However, Controller still depending of the JSON library adopted (_json.hpp_).
On the contrary, the other objects (i.e., HTTPServer, HTTPRequest, HTTPResponse and RouteHandler) are dependent of the HTTP library adopted.
