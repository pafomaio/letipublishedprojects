# US 04 - To delete a category 

## 1. Requirements Engineering

### 1.1. User Story Description

As a Person, I want to delete an existing category.

### 1.2. Customer Specifications and Clarifications 

**From the specifications document:**

n/a

**From the client clarifications:**

> **Question:** Is your intention to make a 'soft' or a 'hard' delete?  
>
> **Answer:** Sorry, I do not know the difference between a soft and hard delete.

> **Question:** On a hard delete, the category will be removed, and, further, the system does not know that category had existed. On a soft-deleted, the system knows that the deleted category had existed and can recover it later. So, which do you prefer?
>
> **Answer:** Ok! For categories, there is no need to complicate things. So, a hard delete must be done.

### 1.3. Acceptance Criteria

* AC04-1. Category is hard deleted.

### 1.4. Found out Dependencies

* There is a dependency with [US01](../US01/US01.md) since a category must be previously created in order to be deleted.

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * n/a

* Selected data:
    * The category to be deleted


**Output Data:**

* List of Categories to be selected
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

![US04-SSD](US04-SSD.svg)


### 1.7 Other Relevant Remarks

* The deleted category does not exist anymore in the system.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US04-MD](US04-MD.svg)

### 2.2. Other Remarks

n/a

## 3. Design - User Story Realization 

### 3.1. Rationale

n/a


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Person
 * Category

Other software classes (i.e. Pure Fabrication) identified: 
 * DeleteCategoryView  
 * DeleteCategoryController
 * CategoryService
 * CategoryRepository
 * RepositoryFactory
 
**Notice that several recently lectured principles and patterns are being applied now.**

## 3.2. Sequence Diagram (SD)

### 3.2.1. Previous Perspective (using Controllers for a Console UI)

**On this matter, Sprint C add no changes here.**

![US04-SD](US04-SD.svg)

**Doubts:**

* Check doubts already put forward on US02 and US03.

### 3.2.2. REST API Perspective (reusing the domain logic)

**To accomodate the Sprint C requirements.**

![US04-SD-RestAPI](US04-SD-RestAPI.svg)

## 3.3. Class Diagram (CD)

### 3.3.1. Previous Perspective (using Controllers for a Console UI)

![US04-CD](US04-CD.svg)

**Note: private attributes and/or methods were omitted.**

### 3.3.2. REST API Perspective (reusing the domain logic)

**To accomodate the Sprint C requirements.**

Just the new classes are shown.

![US04-CD-RestAPI](US04-CD-RestAPI.svg)


# 4. Tests

Two relevant test scenarios are highlighted next.
Other test were also specified.

**Test 1:** Trying to remove a non-existing category. 

    TEST_F(CategoryListRepositoryFixture, RemovingNonExistingCategory){

        this->populateWithFourCategories();
    
        shared_ptr<Category> obj = make_shared<Category>(L"C0099", L"Category 99");
        EXPECT_FALSE(this->repo->remove(obj).isOK());
        EXPECT_EQ(this->repo->count(),4);
    }

**Test 2:** Removing an existing category successfully. 

     TEST_F(CategoryListRepositoryFixture, RemovingCategory){
    
        this->populateWithFourCategories();
    
        EXPECT_TRUE(this->repo->remove(cat2).isOK());
        EXPECT_EQ(this->repo->count(),3);
        optional<shared_ptr<Category>> obj = this->repo->getById(L"C020");
        EXPECT_FALSE(obj.has_value());
    }
    

**Test 3:** Trying to remove twice the same category should fail at the second try.

    TEST_F(CategoryListRepositoryFixture, RemovingNonExistingCategory){
    
        this->populateWithFourCategories();
    
        shared_ptr<Category> obj = make_shared<Category>(L"C0099", L"Category 99");
        EXPECT_FALSE(this->repo->remove(obj).isOK());
        EXPECT_EQ(this->repo->count(),4);
    }


# 5. Integration and Demo 

A menu option on the console application was added. Such option invokes the DeleteCategoryView.


    int CategoriesMenuView::processMenuOption(int option) {
        int result = 0;
        BaseView * view;
        switch (option) {
          ...
          case 4:
            view = new DeleteCategoryView(this->userToken);
            view->show();
            break;
          ...
        }
        return result;
    }

# 6. Observations

n/a





