# README #

This repository contains didactic artifacts to support and demonstrate some principles, concepts, approaches, methods and practices lectured in the [Software Engineering (ESOFT)](https://portal.isep.ipp.pt/intranet/education/visualiza_ficha_uc_v7.aspx?cde=43326) course unit of the [Bachelor in Telecommunications and Informatics Engineering (LETI)](https://www.isep.ipp.pt/Course/Course/473) from [Instituto Superior de Engenharia do Porto (ISEP)](http://www.isep.ipp.pt).

### What is this repository for? ###

This repository aims to be used by students as an **entry point** to their _initial studies_ regarding ESOFT.

For that purpose, it comprehends some demo projects to be exploited throughout the semester.


### How do I get set up? ###

* Download/Clone the repository and open one of the available projects
* Open it using [CLion](https://www.jetbrains.com/clion/)
* All projects are configured/compiled using [CMake](https://cmake.org) files.
    * A quick CMake tutorial can be found [here](https://www.jetbrains.com/help/clion/quick-cmake-tutorial.html).
* For convenience in reading/editing UML artifacts, it is required to install the [PlantUML plugin](https://plugins.jetbrains.com/plugin/7017-plantuml-integration).
* The project is loaded and ready to be compiled and executed.

    
### Who do I talk to? ###

* Paulo Maio (pam@isep.ipp.pt)
